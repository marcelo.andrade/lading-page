@extends('site.layout.app')

@section('title','Landing Page Laravel 5.5')

@section('content')
    <form action="#">
        <fieldset class="clearfix">
            <legend>Personal information:</legend>
            <div class="form-group">
                <div class="col-md-6">
                    <label for="name">Name: </label>
                    <input type="text" name="name" placeholder="Nome" class="form-control">
                </div>
                <div class="col-md-6">
                    <label for="email">E-mail: </label>
                    <input type="email" name="email" placeholder="E-mail" class="form-control">
                </div>
            </div>
        </fieldset>
    </form>
@stop
